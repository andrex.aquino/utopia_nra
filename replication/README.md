# Replication package

The folder `utopia` contains a makefile to run a set of experiments on a dataset of formulas produced by means of the Gk-tail model checker. 

The dataset can be downloaded [here](https://drive.google.com/open?id=0B5BAqcpCRvEPNzBGc1Rnd0YyMjQ).


The provided makefile assumes the data of the given benchmark to be located in a folder named `data` contained in the parent folder of this directory. Beware that running all experiments can take a long time (even hours depending on the machine). The experiments can be parallelized invoking the command: 

    make -j NUMBER_OF_THREADS

