import json
import os
import sys


def head_lines(f, n=1):
    """Returns the first `n` lines of file `f`"""
    if n <= 0:
        return []

    result = []
    with open(f, 'r') as fh:
        for line in fh:
            result.append(line)
            n -= 1
            if n <= 0:
                break
    return result

def tail_lines(f, n=1):
    """Returns the last `n` lines of file `f`"""
    if n <= 0:
        return []

    result = []
    with open(f, 'rb') as fh:
        fh.seek(-2, os.SEEK_END)
        while fh.read(1) != b'\n':
            fh.seek(-2, os.SEEK_CUR)
        
        for _ in xrange(n-1):
            fh.seek(-2, 1)
            while fh.read(1) != b'\n':
                fh.seek(-2, os.SEEK_CUR)

        result = fh.readlines()
    return result


if __name__ == '__main__':
    pname, args = sys.argv[0], sys.argv[1:]
    
    if len(args) != 2:
        print 'Usage: python {} log-file flags'.format(pname)
        exit(1)

    logfile = args[0]
    flags = args[1]

    # Extract the last two lines of the log-file.
    config_line = head_lines(logfile, n=1)[0]
    cache_line, summary_line = tail_lines(logfile, n=2)

    # Transform them into dictionaries for easier access.
    config = json.loads(config_line)
    cache_stats = json.loads(cache_line)
    summary_stats = json.loads(summary_line)

    result = []

    # If the flag `p` is set show the dataset name.
    if 'p' in flags:
        dataset = config['config']['dataset']
        result.append(dataset.split('/')[-1].split('.')[0])

    # If the flag `c` is set show the number of checks performed.
    if 'c' in flags:
        result.append(str(cache_stats['checks']))

    # If the flag `h` is set show the hit count.
    # The hit count is the sum of the hits registered by
    #   1. the hash cache,
    #   2. the sat cache,
    #   3. the unsat cache.
    if 'h' in flags:
        result.append(
            '={}+{}+{}'.format(
                cache_stats['hash_cache_hits'],
                cache_stats['sat_utopia_cache_hits'],
                cache_stats['unsat_utopia_cache_hits']
            )
        )

    # If the flag `d` is set show the decoding time.
    if 'd' in flags:
        t = float(summary_stats['time']['decoder']) / 1000000.0
        result.append(str(t))

    # If the flag `s` is set show the solving time.
    if 's' in flags:
        t = float(summary_stats['time']['solver']) / 1000000.0
        result.append(str(t))

    print '\t'.join(result)