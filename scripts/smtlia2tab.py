import re
import sys


def is_int(n):
    try:
        int(n)
        return True
    except:
        return False

def get_term(i, tokens):
    for index,t in enumerate(tokens):
        if t == "*":
            i -= 1
        if i<0:
            break
    term = []
    for k in xrange(index+1, len(tokens)-1):
        if tokens[k] == "*":
            break
        term.append(tokens[k])
    return term

def smtlia2tab(f):
    result = ""

    lines = f.split("\n")
    decls = filter(lambda line: "declare-fun" in line, lines)
    asserts = filter(lambda line: "assert" in line, lines)

    vars = map(lambda decl: decl.split()[1], decls)

    assert all([e.startswith("x") for e in vars])

    tab_c = len(asserts)
    tab_v = len(set(vars))
    result += "{} {}\n".format(tab_c, tab_v)
    
    for _assert in asserts:
        tab_ineqs = 1
        
        _assert = re.sub("[()]", "", _assert)
        tokens = _assert.split()
        
        tab_comp = tokens[1]
        if tab_comp == "<":
            tab_comp = "lt"
        elif tab_comp == "<=":
            tab_comp = "le"
        elif tab_comp == ">":
            tab_comp = "gt"
        elif tab_comp == ">=":
            tab_comp = "ge"
        elif tab_comp == "=":
            tab_comp = "eq"
        elif tab_comp == "distinct":
            tab_comp = "ne"
        else:
            # This should never happen.
            assert(False)
        
        tab_fc = tokens[-1]
        tab_nterms = len(filter(lambda t: t=="*", tokens))
        
        result += "{} {} {} {}".format(tab_ineqs, tab_comp, tab_fc, tab_nterms)
        
        for i in xrange(tab_nterms):
            term = get_term(i, tokens)
            
            assert is_int(term[0])
            assert all([e.startswith("x") for e in term[1:]])
            
            tab_nvars = len(filter(lambda t: t.startswith("x"), term))
            tab_coeff = term[0]
            tab_vars = " ".join(term[1:])
            result += " {} {} {}".format(tab_nvars, tab_coeff, tab_vars)

        result += "\n"

    return result

def usage(pname):
    return "Usage: python {} smt_dataset_file".format(pname)

if __name__ == '__main__':
    pname, args = sys.argv[0], sys.argv[1:]
    if len(args) != 1:
        print usage(pname)
        exit(-1)

    fh = open(sys.argv[1], "r")
    formulas = fh.read().split("\n\n")
    fh.close()

    for formula in formulas:
        print smtlia2tab(formula)