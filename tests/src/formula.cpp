#define BOOST_TEST_MODULE formula

#include <boost/test/included/unit_test.hpp>
#include <fstream> // includes std::ifstream.
#include <memory> // includes std::shared_ptr.
#include <sstream> // includes std::stringstream.
#include <string>
#include <vector>

#include "../src/core/formula/formula.h"


std::shared_ptr<Model> constModel(std::size_t nr_vars, mpq_class value) {
    std::map<std::size_t, mpq_class> map;
    for (std::size_t i=0; i<nr_vars; ++i) {
        map[i] = value;
    }
    return std::make_shared<Model>(map);
}

std::shared_ptr<Model> idModel(std::size_t nr_vars) {
    std::map<std::size_t, mpq_class> map;
    for (std::size_t i=0; i<nr_vars; ++i) {
        map[i] = i;
    }
    return std::make_shared<Model>(map);
}

std::shared_ptr<Model> invIdModel(std::size_t nr_vars) {
    std::map<std::size_t, mpq_class> map;
    for (std::size_t i=0; i<nr_vars; ++i) {
        mpq_class value = i;
        value *= -1;
        map[i] = value;
    }
    return std::make_shared<Model>(map);
}

std::shared_ptr<Model> succModel(std::size_t nr_vars) {
    std::map<std::size_t, mpq_class> map;
    for (std::size_t i=0; i<nr_vars; ++i) {
        map[i] = i+1;
    }
    return std::make_shared<Model>(map);
}

std::shared_ptr<Model> invSuccModel(std::size_t nr_vars) {
    std::map<std::size_t, mpq_class> map;
    for (std::size_t i=0; i<nr_vars; ++i) {
        mpq_class value = i+1;
        value *= -1;
        map[i] = value;
    }
    return std::make_shared<Model>(map);
}


// Const.

BOOST_AUTO_TEST_CASE(const_get_value_test) {
    Const c0(0);
    Const c1(1);
    Const c2(2);
    Const cm1(-1);
    Const cm2(-2);
    
    BOOST_CHECK_EQUAL(c0.getValue(), 0);
    BOOST_CHECK_EQUAL(c1.getValue(), 1);
    BOOST_CHECK_EQUAL(c2.getValue(), 2);
    BOOST_CHECK_EQUAL(cm1.getValue(), -1);
    BOOST_CHECK_EQUAL(cm2.getValue(), -2);
}

BOOST_AUTO_TEST_CASE(const_subs_test) {
    Const c0(0);
    Const c1(1);
    Const c2(2);
    Const cm1(-1);
    Const cm2(-2);
    
    BOOST_CHECK_EQUAL(c0.subs(nullptr), 0);
    BOOST_CHECK_EQUAL(c1.subs(nullptr), 1);
    BOOST_CHECK_EQUAL(c2.subs(nullptr), 2);
    BOOST_CHECK_EQUAL(cm1.subs(nullptr), -1);
    BOOST_CHECK_EQUAL(cm2.subs(nullptr), -2);
}

BOOST_AUTO_TEST_CASE(const_to_smt_test) {
    Const c0(0);
    Const c1(1);
    Const c2(2);
    Const cm1(-1);
    Const cm2(-2);
    
    BOOST_CHECK_EQUAL(c0.toSmt(), std::string("(to_real 0)"));
    BOOST_CHECK_EQUAL(c1.toSmt(), std::string("(to_real 1)"));
    BOOST_CHECK_EQUAL(c2.toSmt(), std::string("(to_real 2)"));
    BOOST_CHECK_EQUAL(cm1.toSmt(), std::string("(to_real (- 1))"));
    BOOST_CHECK_EQUAL(cm2.toSmt(), std::string("(to_real (- 2))"));
}

BOOST_AUTO_TEST_CASE(const_to_sexpr_test) {
    Const c0(0);
    Const c1(1);
    Const c2(2);
    Const cm1(-1);
    Const cm2(-2);
    
    BOOST_CHECK_EQUAL(c0.toSexpr(), std::string("c 0"));
    BOOST_CHECK_EQUAL(c1.toSexpr(), std::string("c 1"));
    BOOST_CHECK_EQUAL(c2.toSexpr(), std::string("c 2"));
    BOOST_CHECK_EQUAL(cm1.toSexpr(), std::string("c -1"));
    BOOST_CHECK_EQUAL(cm2.toSexpr(), std::string("c -2"));
}

// Var.

BOOST_AUTO_TEST_CASE(var_get_index_test) {
    Var v0(0);
    Var v1(1);
    Var v2(2);
    
    BOOST_CHECK_EQUAL(v0.getIndex(), 0);
    BOOST_CHECK_EQUAL(v1.getIndex(), 1);
    BOOST_CHECK_EQUAL(v2.getIndex(), 2);
}

BOOST_AUTO_TEST_CASE(var_subs_test) {
    Var v0(0);
    Var v1(1);
    Var v2(2);
    
    std::shared_ptr<Model> m_all_zero = constModel(3, 0); // i -> 0
    std::shared_ptr<Model> m_all_one = constModel(3, 1); // i -> 1
    std::shared_ptr<Model> m_all_two = constModel(3, 2); // i -> 2
    std::shared_ptr<Model> m_id = idModel(3); // i -> i
    std::shared_ptr<Model> m_inv_id = invIdModel(3); // i -> -i
    
    BOOST_CHECK_EQUAL(v0.subs(m_all_zero), 0);
    BOOST_CHECK_EQUAL(v0.subs(m_all_one), 1);
    BOOST_CHECK_EQUAL(v0.subs(m_all_two), 2);
    BOOST_CHECK_EQUAL(v1.subs(m_all_zero), 0);
    BOOST_CHECK_EQUAL(v1.subs(m_all_one), 1);
    BOOST_CHECK_EQUAL(v1.subs(m_all_two), 2);
    BOOST_CHECK_EQUAL(v2.subs(m_all_zero), 0);
    BOOST_CHECK_EQUAL(v2.subs(m_all_one), 1);
    BOOST_CHECK_EQUAL(v2.subs(m_all_two), 2);
    BOOST_CHECK_EQUAL(v0.subs(m_id), 0);
    BOOST_CHECK_EQUAL(v1.subs(m_id), 1);
    BOOST_CHECK_EQUAL(v2.subs(m_id), 2);
    BOOST_CHECK_EQUAL(v0.subs(m_inv_id), 0);
    BOOST_CHECK_EQUAL(v1.subs(m_inv_id), -1);
    BOOST_CHECK_EQUAL(v2.subs(m_inv_id), -2);
}

BOOST_AUTO_TEST_CASE(var_to_smt_test) {
    Var v0(0);
    Var v1(1);
    Var v2(2);
    
    BOOST_CHECK_EQUAL(v0.toSmt(), std::string("x0"));
    BOOST_CHECK_EQUAL(v1.toSmt(), std::string("x1"));
    BOOST_CHECK_EQUAL(v2.toSmt(), std::string("x2"));
}

BOOST_AUTO_TEST_CASE(var_to_sexpr_test) {
    Var v0(0);
    Var v1(1);
    Var v2(2);
    
    BOOST_CHECK_EQUAL(v0.toSexpr(), std::string("v 0"));
    BOOST_CHECK_EQUAL(v1.toSexpr(), std::string("v 1"));
    BOOST_CHECK_EQUAL(v2.toSexpr(), std::string("v 2"));
}

// Add.

BOOST_AUTO_TEST_CASE(add_subs_test) {
    // x0 + x1 + 1
    Add add(
        std::shared_ptr<Add>(
            new Add(
                std::shared_ptr<Var>(new Var(0)),
                std::shared_ptr<Var>(new Var(1))
            )
        ),
        std::shared_ptr<Const>(new Const(1))
    );
    
    std::shared_ptr<Model> succ_model = succModel(2); // i -> i+1
    std::shared_ptr<Model> inv_succ_model = invSuccModel(2); // i -> -(i+1)
    
    BOOST_CHECK_EQUAL(add.subs(succ_model), 4);
    BOOST_CHECK_EQUAL(add.subs(inv_succ_model), -2);
}

BOOST_AUTO_TEST_CASE(add_to_smt_test) {
    // 2 + -3 + x0 + x1
    Add add(
        std::shared_ptr<Add>(
            new Add(
                std::shared_ptr<Add>(
                    new Add(
                        std::shared_ptr<Const>(new Const(2)),
                        std::shared_ptr<Const>(new Const(-3))
                    )
                ),
                std::shared_ptr<Var>(new Var(0))
            )
        ),
        std::shared_ptr<Var>(new Var(1))
    );
    
    BOOST_CHECK_EQUAL(add.toSmt(), std::string("(+ (+ (+ (to_real 2) (to_real (- 3))) x0) x1)"));
}

BOOST_AUTO_TEST_CASE(add_to_sexpr_test) {
    // 2 + -3 + x0 + x1
    Add add(
        std::shared_ptr<Add>(
            new Add(
                std::shared_ptr<Add>(
                    new Add(
                        std::shared_ptr<Const>(new Const(2)),
                        std::shared_ptr<Const>(new Const(-3))
                    )
                ),
                std::shared_ptr<Var>(new Var(0))
            )
        ),
        std::shared_ptr<Var>(new Var(1))
    );
    
    BOOST_CHECK_EQUAL(add.toSexpr(), std::string("+ 2 + 2 + 2 c 2 c -3 v 0 v 1"));
}

// Mul.

BOOST_AUTO_TEST_CASE(mul_subs_test) {
    // 2 * -3 * x0
    Mul mul(
        std::shared_ptr<Const>(new Const(2)),
        std::shared_ptr<Mul>(
            new Mul(
                std::shared_ptr<Const>(new Const(-3)),
                std::shared_ptr<Var>(new Var(0))
            )
        )
    );
    
    std::shared_ptr<Model> succ_model = succModel(1); // i -> i+1
    std::shared_ptr<Model> inv_succ_model = invSuccModel(1); // i -> -(i+1)
    
    BOOST_CHECK_EQUAL(mul.subs(succ_model), -6);
    BOOST_CHECK_EQUAL(mul.subs(inv_succ_model), 6);
}

BOOST_AUTO_TEST_CASE(mul_to_smt_test) {
    // 2 * -3 * x0
    Mul mul(
        std::shared_ptr<Const>(new Const(2)),
        std::shared_ptr<Mul>(
            new Mul(
                std::shared_ptr<Const>(new Const(-3)),
                std::shared_ptr<Var>(new Var(0))
            )
        )
    );
    
    BOOST_CHECK_EQUAL(mul.toSmt(), std::string("(* (to_real 2) (* (to_real (- 3)) x0))"));
}

BOOST_AUTO_TEST_CASE(mul_to_sexpr_test) {
    // 2 * -3 * x0
    Mul mul(
        std::shared_ptr<Const>(new Const(2)),
        std::shared_ptr<Mul>(
            new Mul(
                std::shared_ptr<Const>(new Const(-3)),
                std::shared_ptr<Var>(new Var(0))
            )
        )
    );
    
    BOOST_CHECK_EQUAL(mul.toSexpr(), std::string("* 2 c 2 * 2 c -3 v 0"));
}

// Inequality.

BOOST_AUTO_TEST_CASE(ineq_subs_test) {
    // 3 * x0 + 2 * x1 + 1 * x2
    std::shared_ptr<Add> add(
        new Add(
            std::shared_ptr<Mul>(
                new Mul(
                    std::shared_ptr<Const>(new Const(3)),
                    std::shared_ptr<Var>(new Var(0))
                )
            ),
            std::shared_ptr<Add>(
                new Add(
                    std::shared_ptr<Mul>(
                        new Mul(
                            std::shared_ptr<Const>(new Const(2)),
                            std::shared_ptr<Var>(new Var(1))
                        )
                    ),
                    std::shared_ptr<Mul>(
                        new Mul(
                            std::shared_ptr<Const>(new Const(1)),
                            std::shared_ptr<Var>(new Var(2))
                        )
                    )
                )
            )
        )
    );
    
    std::shared_ptr<Const> zero_fc(new Const(0));
    
    Inequality lt_ineq(add, zero_fc, Comparison::LT);
    Inequality le_ineq(add, zero_fc, Comparison::LE);
    Inequality gt_ineq(add, zero_fc, Comparison::GT);
    Inequality ge_ineq(add, zero_fc, Comparison::GE);
    Inequality eq_ineq(add, zero_fc, Comparison::EQ);
    Inequality ne_ineq(add, zero_fc, Comparison::NE);
    
    std::shared_ptr<Model> all_zero_model = constModel(3, 0);
    std::shared_ptr<Model> succ_model = succModel(3); // i -> i+1
    std::shared_ptr<Model> inv_succ_model = invSuccModel(3); // i -> -(i+1)
    
    BOOST_CHECK(!lt_ineq.subs(all_zero_model));
    BOOST_CHECK(!lt_ineq.subs(succ_model));
    BOOST_CHECK(lt_ineq.subs(inv_succ_model));
    BOOST_CHECK(le_ineq.subs(all_zero_model));
    BOOST_CHECK(!le_ineq.subs(succ_model));
    BOOST_CHECK(le_ineq.subs(inv_succ_model));
    BOOST_CHECK(!gt_ineq.subs(all_zero_model));
    BOOST_CHECK(gt_ineq.subs(succ_model));
    BOOST_CHECK(!gt_ineq.subs(inv_succ_model));
    BOOST_CHECK(ge_ineq.subs(all_zero_model));
    BOOST_CHECK(ge_ineq.subs(succ_model));
    BOOST_CHECK(!ge_ineq.subs(inv_succ_model));
    BOOST_CHECK(eq_ineq.subs(all_zero_model));
    BOOST_CHECK(!eq_ineq.subs(succ_model));
    BOOST_CHECK(!eq_ineq.subs(inv_succ_model));
    BOOST_CHECK(!ne_ineq.subs(all_zero_model));
    BOOST_CHECK(ne_ineq.subs(succ_model));
    BOOST_CHECK(ne_ineq.subs(inv_succ_model));
}

/*

BOOST_AUTO_TEST_CASE(ineq_satdelta_test) {
    // x0 / x1
    std::shared_ptr<Div> div(
        new Div(
            std::shared_ptr<Var>(new Var(0)),
            std::shared_ptr<Var>(new Var(1))
        )
    );
    
    std::shared_ptr<Const> zero_fc(new Const(0));
    Inequality lt_ineq(div, Comparison::lt, zero_fc);
    Inequality le_ineq(div, Comparison::le, zero_fc);
    Inequality gt_ineq(div, Comparison::gt, zero_fc);
    Inequality ge_ineq(div, Comparison::ge, zero_fc);
    Inequality eq_ineq(div, Comparison::eq, zero_fc);
    Inequality ne_ineq(div, Comparison::ne, zero_fc);
    
    std::shared_ptr<Model> m_all_zero = Model::constModel(0,2);
    std::shared_ptr<Model> m_all_one = Model::constModel(1,2);
    std::shared_ptr<Model> m_id = Model::fromFunction(id, 2);
    
    BOOST_CHECK_THROW(ne_ineq.satDelta(m_all_zero), InvalidOperation);
    BOOST_CHECK_EQUAL(lt_ineq.satDelta(m_all_one), 2);
    BOOST_CHECK_EQUAL(le_ineq.satDelta(m_all_one), 1);
    BOOST_CHECK_EQUAL(gt_ineq.satDelta(m_all_one), 0);
    BOOST_CHECK_EQUAL(ge_ineq.satDelta(m_all_one), 0);
    BOOST_CHECK_EQUAL(eq_ineq.satDelta(m_all_one), 1);
    BOOST_CHECK_EQUAL(ne_ineq.satDelta(m_all_one), 0);
    BOOST_CHECK_EQUAL(lt_ineq.satDelta(m_id), 1);
    BOOST_CHECK_EQUAL(le_ineq.satDelta(m_id), 0);
    BOOST_CHECK_EQUAL(gt_ineq.satDelta(m_id), 1);
    BOOST_CHECK_EQUAL(ge_ineq.satDelta(m_id), 0);
    BOOST_CHECK_EQUAL(eq_ineq.satDelta(m_id), 0);
    BOOST_CHECK_EQUAL(ne_ineq.satDelta(m_id), 1);
}

// Clause.

BOOST_AUTO_TEST_CASE(clause_subs_test) {
    // x0 < 0
    std::shared_ptr<Inequality> lt_ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::lt,
            std::shared_ptr<Const>(new Const(0))
        )
    );
    
    // x0 = 0
    std::shared_ptr<Inequality> eq_ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::eq,
            std::shared_ptr<Const>(new Const(0))
        )
    );
    
    Clause clause1; // x0 < 0
    Clause clause2; // x0 < 0 or x0 = 0, that is, x0 <= 0
    
    clause1.addInequality(lt_ineq);
    clause2.addInequality(lt_ineq);
    clause2.addInequality(eq_ineq);
    
    std::shared_ptr<Model> m_all_zero = Model::constModel(0,1);
    std::shared_ptr<Model> m_all_one = Model::constModel(1,1);
    std::shared_ptr<Model> m_all_minus_one = Model::constModel(-1,1);
    
    BOOST_CHECK(not clause1.subs(m_all_zero));
    BOOST_CHECK(clause2.subs(m_all_zero));
    BOOST_CHECK(not clause1.subs(m_all_one));
    BOOST_CHECK(not clause2.subs(m_all_one));
    BOOST_CHECK(clause1.subs(m_all_minus_one));
    BOOST_CHECK(clause2.subs(m_all_minus_one));
}

BOOST_AUTO_TEST_CASE(clause_satdelta_test) {
    // x0 < 2
    std::shared_ptr<Inequality> lt2_ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::lt,
            std::shared_ptr<Const>(new Const(2))
        )
    );
    
    // x0 > 8
    std::shared_ptr<Inequality> gt8_ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::gt,
            std::shared_ptr<Const>(new Const(8))
        )
    );
    
    Clause clause; // x0 < 2 or x0 > 8
    
    clause.addInequality(lt2_ineq);
    clause.addInequality(gt8_ineq);
    
    std::shared_ptr<Model> m_all_zero = Model::constModel(0,1);
    std::shared_ptr<Model> m_all_four = Model::constModel(4,1);
    std::shared_ptr<Model> m_all_six = Model::constModel(6,1);
    std::shared_ptr<Model> m_all_ten = Model::constModel(10,1);
    
    BOOST_CHECK_EQUAL(clause.satDelta(m_all_zero), 0);
    BOOST_CHECK_EQUAL(clause.satDelta(m_all_four), 3);
    BOOST_CHECK_EQUAL(clause.satDelta(m_all_six), 3);
    BOOST_CHECK_EQUAL(clause.satDelta(m_all_ten), 0);
}

BOOST_AUTO_TEST_CASE(clause_count_sat_ineqs_test) {
    // x0 < 2
    std::shared_ptr<Inequality> lt2_ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::lt,
            std::shared_ptr<Const>(new Const(2))
        )
    );
    
    // x0 > 0
    std::shared_ptr<Inequality> gt0_ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::gt,
            std::shared_ptr<Const>(new Const(0))
        )
    );
    
    // x0 > 8
    std::shared_ptr<Inequality> gt8_ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::gt,
            std::shared_ptr<Const>(new Const(8))
        )
    );
    
    Clause clause1; // x0 < 2 or x0 > 8
    Clause clause2; // x0 > 0 or x0 < 2
    
    clause1.addInequality(lt2_ineq);
    clause1.addInequality(gt8_ineq);
    clause2.addInequality(gt0_ineq);
    clause2.addInequality(lt2_ineq);
    
    std::shared_ptr<Model> m_all_zero = Model::constModel(0,1);
    std::shared_ptr<Model> m_all_one = Model::constModel(1,1);
    std::shared_ptr<Model> m_all_five = Model::constModel(5,1);
    std::shared_ptr<Model> m_all_ten = Model::constModel(10,1);
    
    BOOST_CHECK_EQUAL(clause1.countSatisfiedInequalities(m_all_zero), 1);
    BOOST_CHECK_EQUAL(clause1.countSatisfiedInequalities(m_all_one), 1);
    BOOST_CHECK_EQUAL(clause1.countSatisfiedInequalities(m_all_five), 0);
    BOOST_CHECK_EQUAL(clause1.countSatisfiedInequalities(m_all_ten), 1);
    
    BOOST_CHECK_EQUAL(clause2.countSatisfiedInequalities(m_all_zero), 1);
    BOOST_CHECK_EQUAL(clause2.countSatisfiedInequalities(m_all_one), 2);
    BOOST_CHECK_EQUAL(clause2.countSatisfiedInequalities(m_all_five), 1);
    BOOST_CHECK_EQUAL(clause2.countSatisfiedInequalities(m_all_ten), 1);
}

// Formula.

BOOST_AUTO_TEST_CASE(formula_from_sexpr_test) {
    std::ifstream sf_file("../data/small_formula.sexpr");
    std::ifstream lf_file("../data/large_formula.sexpr");
    std::ifstream missing_file("../data/a_missing_file.sexpr");
    
    BOOST_REQUIRE(sf_file.good());
    BOOST_REQUIRE(lf_file.good());
    BOOST_REQUIRE(not missing_file.good());
    
    std::shared_ptr<Formula> sf = Formula::fromSexpr(sf_file);
    std::shared_ptr<Formula> lf = Formula::fromSexpr(lf_file);
    std::shared_ptr<Formula> missing = Formula::fromSexpr(missing_file);
    
    BOOST_REQUIRE(sf != nullptr);
    BOOST_REQUIRE(lf != nullptr);
    BOOST_REQUIRE(missing == nullptr);
    
    BOOST_CHECK_EQUAL(sf->numberOfClauses(), 3);
    BOOST_CHECK_EQUAL(sf->numberOfVars(), 2);
    BOOST_CHECK_EQUAL(lf->numberOfClauses(), 15);
    BOOST_CHECK_EQUAL(lf->numberOfVars(), 10);
}

BOOST_AUTO_TEST_CASE(formula_from_and_to_sexpr_test) {
    // Decode two formulas from the given files.
    std::ifstream sf_file1("../data/small_formula.sexpr");
    std::ifstream lf_file1("../data/large_formula.sexpr");
    
    BOOST_REQUIRE(sf_file1.good());
    BOOST_REQUIRE(lf_file1.good());
    
    std::shared_ptr<Formula> sf = Formula::fromSexpr(sf_file1);
    std::shared_ptr<Formula> lf = Formula::fromSexpr(lf_file1);
    
    sf_file1.close();
    lf_file1.close();
    
    BOOST_REQUIRE(sf != nullptr);
    BOOST_REQUIRE(lf != nullptr);
    
    // Reopen the files and copy them to two string-streams.
    std::ifstream sf_file2("../data/small_formula.sexpr");
    std::ifstream lf_file2("../data/large_formula.sexpr");
    
    BOOST_REQUIRE(sf_file2.good());
    BOOST_REQUIRE(lf_file2.good());
    
    std::ostringstream sf_ss, lf_ss;
    sf_ss << sf_file2.rdbuf();
    lf_ss << lf_file2.rdbuf();
    sf_file2.close();
    lf_file2.close();
    
    BOOST_CHECK_EQUAL(sf->toSexpr(), sf_ss.str());
    BOOST_CHECK_EQUAL(lf->toSexpr(), lf_ss.str());
}

BOOST_AUTO_TEST_CASE(formula_random_test) {
    const std::size_t MAX_CLAUSES = 20;
    const std::size_t MAX_INEQS = 15;
    const std::size_t MAX_VARS = 10;
    const std::size_t MAX_DEPTH = 5;
    const std::size_t MAX_WIDTH = 3;
    const int MIN_CONST = -100;
    const int MAX_CONST = 100;

    std::shared_ptr<Formula> formula = randomFormula(
        Logic::QFLIA,
        MAX_CLAUSES,
        MAX_INEQS,
        MAX_VARS,
        MAX_DEPTH,
        MAX_WIDTH,
        MIN_CONST,
        MAX_CONST
    );
    
    BOOST_CHECK_LE(formula->numberOfClauses(), MAX_CLAUSES);
    BOOST_CHECK_LE(formula->numberOfVars(), MAX_VARS);
    
    const std::vector<std::shared_ptr<Clause> > &clauses = formula->getClauses();
    for (auto clause_it=clauses.begin(); clause_it!=clauses.end(); ++clause_it) {
        BOOST_CHECK_LE((*clause_it)->numberOfInequalities(), MAX_INEQS);
    }
}

BOOST_AUTO_TEST_CASE(formula_with_gaps_to_smt_test) {
    std::shared_ptr<Formula> formula(new Formula());
    std::shared_ptr<Clause> clause(new Clause());
    std::shared_ptr<Inequality> ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(5)),
            Comparison::lt,
            std::shared_ptr<Const>(new Const(0))
        )
    );
    clause->addInequality(ineq);
    formula->addClause(clause);
    
    BOOST_CHECK_EQUAL(formula->numberOfClauses(), 1);
    BOOST_CHECK_EQUAL(formula->numberOfVars(), 1);
    BOOST_CHECK_EQUAL(formula->toSmt(), std::string("(declare-fun x5 () Int)\n(assert (< x5 0))"));
}

BOOST_AUTO_TEST_CASE(formula_depth_and_width_test) {
    // formula = x0 < ((x1 % 3) / 2)
    std::shared_ptr<Formula> formula(new Formula());
    std::shared_ptr<Clause> clause(new Clause());
    std::shared_ptr<Inequality> ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::lt,
            std::shared_ptr<Div>(
                new Div(
                    std::shared_ptr<Mod>(
                        new Mod(
                            std::shared_ptr<Var>(new Var(1)),
                            std::shared_ptr<Const>(new Const(3))
                        )
                    ),
                    std::shared_ptr<Const>(new Const(2))
                )
            )
        )
    );
    clause->addInequality(ineq);
    formula->addClause(clause);
    
    BOOST_CHECK_EQUAL(formula->depth(), 6);
    BOOST_CHECK_EQUAL(formula->width(), 4);
}

BOOST_AUTO_TEST_CASE(formula_get_vars_test) {
    // formula = x0 < ((x3 % 3) / 2)
    std::shared_ptr<Formula> formula(new Formula());
    std::shared_ptr<Clause> clause(new Clause());
    std::shared_ptr<Inequality> ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::lt,
            std::shared_ptr<Div>(
                new Div(
                    std::shared_ptr<Mod>(
                        new Mod(
                            std::shared_ptr<Var>(new Var(3)),
                            std::shared_ptr<Const>(new Const(3))
                        )
                    ),
                    std::shared_ptr<Const>(new Const(2))
                )
            )
        )
    );
    clause->addInequality(ineq);
    formula->addClause(clause);
    
    std::set<std::size_t> vars;
    formula->getVars(vars);
    
    BOOST_CHECK_EQUAL(vars.size(), 2);
    BOOST_CHECK(vars.find(0) != vars.end());
    BOOST_CHECK(vars.find(3) != vars.end());
    BOOST_CHECK(vars.find(1) == vars.end());
}

BOOST_AUTO_TEST_CASE(formula_compress_test) {
    // formula = x0 < ((x3 % 3) / 2)
    std::shared_ptr<Formula> formula(new Formula());
    std::shared_ptr<Clause> clause(new Clause());
    std::shared_ptr<Inequality> ineq(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::lt,
            std::shared_ptr<Div>(
                new Div(
                    std::shared_ptr<Mod>(
                        new Mod(
                            std::shared_ptr<Var>(new Var(3)),
                            std::shared_ptr<Const>(new Const(3))
                        )
                    ),
                    std::shared_ptr<Const>(new Const(2))
                )
            )
        )
    );
    clause->addInequality(ineq);
    formula->addClause(clause);
    
    // Compress the formula.
    // After compression the resulting formula should be:
    // formula = x0 < ((x1 % 3) / 2)
    formula = formula->compress();
    
    std::set<std::size_t> vars;
    formula->getVars(vars);
    
    BOOST_CHECK_EQUAL(vars.size(), 2);
    BOOST_CHECK(vars.find(0) != vars.end());
    BOOST_CHECK(vars.find(1) != vars.end());
    BOOST_CHECK(vars.find(3) == vars.end());
}

BOOST_AUTO_TEST_CASE(formula_slice_test) {
    // formula = (x0 = 0) and (x1 = 0) and (x2 = 0) and (x0 + x1 = 0)
    // It can be sliced into the following independent subformulas:
    //     - (x0 = 0) and (x1 = 0) and (x0 + x1 = 0)
    //     - (x2 = 0)
    std::shared_ptr<Formula> formula(new Formula());
    
    std::shared_ptr<Clause> clause1(new Clause());
    std::shared_ptr<Clause> clause2(new Clause());
    std::shared_ptr<Clause> clause3(new Clause());
    std::shared_ptr<Clause> clause4(new Clause());
    
    std::shared_ptr<Inequality> ineq1(
        new Inequality(
            std::shared_ptr<Var>(new Var(0)),
            Comparison::eq,
            std::shared_ptr<Const>(new Const(0))
        )
    );
    
    std::shared_ptr<Inequality> ineq2(
        new Inequality(
            std::shared_ptr<Var>(new Var(1)),
            Comparison::eq,
            std::shared_ptr<Const>(new Const(0))
        )
    );
    
    std::shared_ptr<Inequality> ineq3(
        new Inequality(
            std::shared_ptr<Var>(new Var(2)),
            Comparison::eq,
            std::shared_ptr<Const>(new Const(0))
        )
    );
    
    std::shared_ptr<Inequality> ineq4(
        new Inequality(
            std::shared_ptr<Add>(
                new Add(
                    std::shared_ptr<Var>(new Var(0)),
                    std::shared_ptr<Var>(new Var(1))
                )
            ),
            Comparison::eq,
            std::shared_ptr<Const>(new Const(0))
        )
    );
    
    clause1->addInequality(ineq1);
    clause2->addInequality(ineq2);
    clause3->addInequality(ineq3);
    clause4->addInequality(ineq4);
    
    formula->addClause(clause1);
    formula->addClause(clause2);
    formula->addClause(clause3);
    formula->addClause(clause4);
    
    std::vector<std::shared_ptr<Formula> > sliced_formulas;
    formula->slice(sliced_formulas);
        
    BOOST_REQUIRE(sliced_formulas.size() == 2);
    
    std::shared_ptr<Formula> sf1 = sliced_formulas[0];
    std::shared_ptr<Formula> sf2 = sliced_formulas[1];
    
    BOOST_CHECK(sf1->numberOfClauses() == 1 || sf1->numberOfClauses() == 3);
    BOOST_CHECK(sf2->numberOfClauses() == 1 || sf2->numberOfClauses() == 3);
    BOOST_CHECK(sf1->numberOfClauses() != sf2->numberOfClauses());
    BOOST_CHECK_EQUAL(
        sf1->toSmt(),
        std::string("")
        + "(declare-fun x0 () Int)\n"
        + "(declare-fun x1 () Int)\n"
        + "(assert (= x0 0))\n"
        + "(assert (= x1 0))\n"
        + "(assert (= (+ x0 x1) 0))"
    );
    BOOST_CHECK_EQUAL(
        sf2->toSmt(),
        std::string("")
        + "(declare-fun x2 () Int)\n"
        + "(assert (= x2 0))"
    );
}
*/