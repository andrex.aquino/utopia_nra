cd ./src

find . -type f -name "*.out" | while read cmd
do
    echo "Running Test Suite: $cmd"
    ./$cmd --log_level=message
done