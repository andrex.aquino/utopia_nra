#ifndef CORE_CACHE_UTOPIA_CACHE_RANDOM_CACHE_RANDOM_CACHE_H
#define CORE_CACHE_UTOPIA_CACHE_RANDOM_CACHE_RANDOM_CACHE_H

#include <memory>
#include <stdlib.h>

#include "commons/utils/random/random.h"
#include "core/formula/formula.h"
#include "core/cache/utopia_cache/utopia_cache.h"


template <typename T>
class RandomCache: public UtopiaCache<T> {
  public:
    RandomCache() {
        srand(0);
    }
    
    // Returns a random number.
    double tier1Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        return randint(0, 100000);
    };

    // Returns a random number.
    double tier2Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        return randint(0, 100000);
    };
};

#endif
