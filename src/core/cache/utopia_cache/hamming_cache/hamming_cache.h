#ifndef CORE_CACHE_UTOPIA_CACHE_HAMMING_CACHE_HAMMING_CACHE_H
#define CORE_CACHE_UTOPIA_CACHE_HAMMING_CACHE_HAMMING_CACHE_H

#include <cstddef>
#include <memory>
#include <vector>

#include "core/formula/formula.h"
#include "core/cache/utopia_cache/utopia_cache.h"


class FormulaFlattener: public FormulaVisitor {
  private:
    std::vector<std::size_t> flat_list;
    
  public:
    const std::vector<std::size_t>& getFlatList() const {
        return this->flat_list;
    };
    
    void onClause(const Clause &clause) {
        this->flat_list.push_back(0);
    };
    
    void onInequality(const Inequality &inequality) {
        switch (inequality.getComparison()) {
            case Comparison::LT:
                this->flat_list.push_back(1);
                break;
            case Comparison::LE:
                this->flat_list.push_back(2);
                break;
            case Comparison::GT:
                this->flat_list.push_back(3);
                break;
            case Comparison::GE:
                this->flat_list.push_back(4);
                break;
            case Comparison::EQ:
                this->flat_list.push_back(5);
                break;
            case Comparison::NE:
                this->flat_list.push_back(6);
                break;
        }
    };
    
    void onAdd(const Add &add) {
        this->flat_list.push_back(7);
    };

    void onMul(const Mul &mul) {
        this->flat_list.push_back(8);
    };
    
    void onDiv(const Div &div) {
        this->flat_list.push_back(9);
    };
    
    void onVar(const Var &v) {
        this->flat_list.push_back(10);
        this->flat_list.push_back(v.getHash());
    };
    
    void onConst(const Const &c) {
        this->flat_list.push_back(11);
        this->flat_list.push_back(c.getHash());
    };
};

template <typename T>
class HammingCache: public UtopiaCache<T> {
  public:
    double tier1Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        return f1->getSize();
    };

    double tier2Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        FormulaFlattener flattener1;
        FormulaFlattener flattener2;
        
        f1->visit(flattener1);
        f2->visit(flattener2);
        
        const std::vector<std::size_t> &fl1 = flattener1.getFlatList();
        const std::vector<std::size_t> &fl2 = flattener2.getFlatList();
        
        std::size_t delta = 0;
        for (std::size_t i=0; i<std::min(fl1.size(), fl2.size()); ++i) {
            if (fl1[i] != fl2[i]) {
                ++delta;
            }
        }
        delta += std::max(fl1.size(), fl2.size()) - std::min(fl1.size(), fl2.size());
        return -1 * static_cast<double>(delta);
    };
};

#endif
