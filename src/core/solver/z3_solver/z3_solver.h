#ifndef CORE_SOLVER_Z3_SOLVER_Z3_SOLVER_H
#define CORE_SOLVER_Z3_SOLVER_Z3_SOLVER_H

#include <map>
#include <memory>
#include <string>

#include "core/formula/formula.h"
#include "core/solver/solver.h"

#include "z3++.h"


class Z3Solver: public Solver {
	private:
		unsigned timeout;

		// Transforms a clause into a Z3 expression in the given context.
		z3::expr clauseToZ3Expr(std::shared_ptr<Clause> clause, z3::context &c) const;

		// Transforms a Z3 model into an instance of Model.
		std::shared_ptr<Model> decodeZ3Model(z3::model &model) const;

		// Transforms a Z3 unsat-core into an instance of UnsatCore.
		std::shared_ptr<UnsatCore> decodeZ3UnsatCore(
            z3::expr_vector &core,
            std::map<std::string, unsigned long> &id_to_clause_map) const;

  public:
    Z3Solver();
    virtual ~Z3Solver();
  
    // Returns the timeout for this solver.
    unsigned getTimeout() const;

    // Modifies the timeout for this solver.
    void setTimeout(unsigned timeout);

    // Returns the satisfiability value of `formula`.
    CheckResult check(std::shared_ptr<Formula> formula);
};

#endif
