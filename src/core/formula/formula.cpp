#include "formula.h"

#include <exception>
#include <sstream>
#include <iostream>

#include "boost/config.hpp"
#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/connected_components.hpp"

#include "commons/utils/set/set.h"


// Solution.

Solution::~Solution() {}


// Model.

Model::Model(std::map<std::size_t, mpq_class> &map) {
	for (auto it=map.begin(); it!=map.end(); ++it) {
		this->map[it->first] = it->second;
	}
}

Model::~Model() {}

std::size_t Model::size() const {
    return this->map.size();
}

mpq_class Model::get(std::size_t index) const {
    auto it = this->map.find(index);
    if (it == this->map.end()) {
        return 0;
    }
    return it->second;
}

bool Model::check(std::shared_ptr<Formula> formula) {
	return formula->sharesModel(shared_from_this());
}

std::string Model::toString() const {
	std::stringstream result;
	result << this->size();
	for (auto it=this->map.begin(); it!=this->map.end(); ++it) {
		result << " " << it->first << " " << it->second;
	}
	return result.str();
}


// Unsat core.

UnsatCore::UnsatCore(std::vector<std::size_t> &clauses) {
	for (auto it=clauses.begin(); it!=clauses.end(); ++it) {
		this->clauses.push_back(*it);
	}
}

UnsatCore::~UnsatCore() {}

std::size_t UnsatCore::size() const {
    return this->clauses.size();
}

const std::vector<std::size_t>& UnsatCore::getClauses() const {
	return this->clauses;
}

bool UnsatCore::check(std::shared_ptr<Formula> formula) {
	return formula->sharesUnsatCore(shared_from_this());
}

std::string UnsatCore::toString() const {
	std::stringstream result;
	result << this->size();
	for (auto it=this->clauses.begin(); it!=this->clauses.end(); ++it) {
		result << " " << (*it);
	}
	return result.str();
}


// Operation error.

OperationError::~OperationError() throw() {}

OperationError::OperationError(std::string message) : message(message) {}

const char* OperationError::what() const throw() {
	return message.c_str();
}


// Const.

Const::Const(mpq_class value) : value(value) {
    this->size = 1;
    
	std::size_t seed = 1;
	boost::hash_combine(seed, value.get_num().get_si());
    boost::hash_combine(seed, value.get_den().get_si());
    this->hash = seed;
}

Const::~Const() {} 

mpq_class Const::getValue() const {
	return this->value;
}

const std::set<std::size_t>& Const::getVars() const {
    return this->vars;
}

mpq_class Const::subs(std::shared_ptr<Model> model) const {
	return this->value;
}

std::shared_ptr<Expr> Const::renameVars(subst_map &subst) const {
	return this->copy();
}

std::string Const::toSmt() const {
	std::stringstream result;
	if (this->value < 0) {
		result << "(- " << abs(this->value) << ")";
	} else {
		result << this->value;
	}
	return "(to_real " + result.str() + ")";
}

std::string Const::toSexpr() const {
	std::stringstream result;
	result << "c " << this->value;
	return result.str();
}

std::shared_ptr<Expr> Const::copy() const {
	std::shared_ptr<Const> c(new Const(this->value));
	return c;
};

void Const::visit(FormulaVisitor &visitor) const {
	visitor.onConst(*this);
};


// Var.

Var::Var(std::size_t index) : index(index) {
    this->vars.insert(index);

    this->size = 1;

	std::size_t seed = 2;
	boost::hash_combine(seed, index);
    this->hash = seed;
}

Var::~Var() {}

std::size_t Var::getIndex() const {
	return this->index;
}

const std::set<std::size_t>& Var::getVars() const {
	return this->vars;
}

mpq_class Var::subs(std::shared_ptr<Model> model) const {
	return model->get(this->index);
}

std::shared_ptr<Expr> Var::renameVars(subst_map &subst) const {
	auto it = subst.find(this->index);
	if (it == subst.end())
		throw OperationError("Missing substitution for variable " + std::to_string(this->index));
	return std::shared_ptr<Expr>(new Var(it->second));
}

std::string Var::toSmt() const {
	return "x" + std::to_string(this->index);
}

std::string Var::toSexpr() const {
	return "v " + std::to_string(this->index);
}

std::shared_ptr<Expr> Var::copy() const {
	std::shared_ptr<Var> var(new Var(this->index));
	return var;
};

void Var::visit(FormulaVisitor &visitor) const {
	visitor.onVar(*this);
};


// Add.

Add::Add(std::shared_ptr<Expr> lhs, std::shared_ptr<Expr> rhs)
        : lhs(lhs), rhs(rhs) {
	    
    this->vars.insert(lhs->getVars().begin(), lhs->getVars().end());
    this->vars.insert(rhs->getVars().begin(), rhs->getVars().end());
        
    this->size = 1;
    this->size += this->lhs->getSize();
    this->size += this->rhs->getSize();
    
    std::size_t seed = 3;
	boost::hash_combine(seed, this->lhs->getHash());
    boost::hash_combine(seed, this->rhs->getHash());
    this->hash = seed;
}

Add::~Add() {}

std::shared_ptr<Expr> Add::getLhs() const {
	return this->lhs;
}

std::shared_ptr<Expr> Add::getRhs() const {
	return this->rhs;
}

const std::set<std::size_t>& Add::getVars() const {
	return this->vars;
}

mpq_class Add::subs(std::shared_ptr<Model> model) const {
	return this->lhs->subs(model) + this->rhs->subs(model);
}

std::shared_ptr<Expr> Add::renameVars(subst_map &subst) const {
	std::shared_ptr<Add> add(
			new Add(
					this->lhs->renameVars(subst),
					this->rhs->renameVars(subst)
			)
	);
	return add;
}

std::string Add::toSmt() const {
	std::string result = "(+ " + this->lhs->toSmt() + " " + this->rhs->toSmt() + ")";
	return result;
}

std::string Add::toSexpr() const {
	std::string result = "+ 2 " + this->lhs->toSexpr() + " " + this->rhs->toSexpr();
	return result;
}

std::shared_ptr<Expr> Add::copy() const {
	std::shared_ptr<Add> add(
			new Add(
					this->lhs->copy(),
					this->rhs->copy()
			)
	);
	return add;
};

void Add::visit(FormulaVisitor &visitor) const {
	visitor.onAdd(*this);
	this->lhs->visit(visitor);
	this->rhs->visit(visitor);
};


// Mul.

Mul::Mul(std::shared_ptr<Expr> lhs, std::shared_ptr<Expr> rhs)
        : lhs(lhs), rhs(rhs) {

    this->vars.insert(
        lhs->getVars().begin(),
        lhs->getVars().end()
    );
    
    this->vars.insert(
        rhs->getVars().begin(),
        rhs->getVars().end()
    );

    this->size = 1;
    this->size += this->lhs->getSize();
    this->size += this->rhs->getSize();

    std::size_t seed = 4;
    boost::hash_combine(seed, this->lhs->getHash());
    boost::hash_combine(seed, this->rhs->getHash());
    this->hash = seed;
}

Mul::~Mul() {}

std::shared_ptr<Expr> Mul::getLhs() const {
	return this->lhs;
}

std::shared_ptr<Expr> Mul::getRhs() const {
	return this->rhs;
}

const std::set<std::size_t>& Mul::getVars() const {
	return this->vars;
}

mpq_class Mul::subs(std::shared_ptr<Model> model) const {
	return this->lhs->subs(model) * this->rhs->subs(model);
}

std::shared_ptr<Expr> Mul::renameVars(subst_map &subst) const {
	std::shared_ptr<Mul> mul(
        new Mul(
            this->lhs->renameVars(subst),
            this->rhs->renameVars(subst)
        )
	);
	return mul;
}

std::string Mul::toSmt() const {
	std::string result = "(* " + this->lhs->toSmt() + " " + this->rhs->toSmt() + ")";
	return result;
}

std::string Mul::toSexpr() const {
	std::string result = "* 2 " + this->lhs->toSexpr() + " " + this->rhs->toSexpr();
	return result;
}

std::shared_ptr<Expr> Mul::copy() const {
	std::shared_ptr<Mul> mul(
        new Mul(
            this->lhs->copy(),
            this->rhs->copy()
        )
	);
	return mul;
};

void Mul::visit(FormulaVisitor &visitor) const {
	visitor.onMul(*this);
	this->lhs->visit(visitor);
	this->rhs->visit(visitor);
};


// Div.

Div::Div(std::shared_ptr<Expr> lhs, std::shared_ptr<Expr> rhs)
        : lhs(lhs), rhs(rhs) {

    this->vars.insert(
        lhs->getVars().begin(),
        lhs->getVars().end()
    );
    
    this->vars.insert(
        rhs->getVars().begin(),
        rhs->getVars().end()
    );

    // Only need to add the size of `expr` since size is initialized to two
    // and the size of the constant term is always one.
    this->size = 1;
    this->size += this->lhs->getSize();
    this->size += this->rhs->getSize();

    std::size_t seed = 4;
    boost::hash_combine(seed, this->lhs->getHash());
    boost::hash_combine(seed, this->rhs->getHash());
    this->hash = seed;
}

Div::~Div() {}

std::shared_ptr<Expr> Div::getLhs() const {
	return this->lhs;
}

std::shared_ptr<Expr> Div::getRhs() const {
	return this->rhs;
}

const std::set<std::size_t>& Div::getVars() const {
	return this->vars;
}

mpq_class Div::subs(std::shared_ptr<Model> model) const {
    mpq_class lhs = this->lhs->subs(model);
    mpq_class rhs = this->rhs->subs(model);
    
    if (rhs == 0) {
        throw OperationError("Model evaluation produced division by zero.");
    }
    
	return lhs / rhs;
}

std::shared_ptr<Expr> Div::renameVars(subst_map &subst) const {
	std::shared_ptr<Div> div(
        new Div(
            this->lhs->renameVars(subst),
            this->rhs->renameVars(subst)
        )
	);
	return div;
}

std::string Div::toSmt() const {
	std::string result = "(/ " + this->lhs->toSmt() + " " + this->rhs->toSmt() + ")";
	return result;
}

std::string Div::toSexpr() const {
	std::string result = "/ " + this->lhs->toSexpr() + " " + this->rhs->toSexpr();
	return result;
}

std::shared_ptr<Expr> Div::copy() const {
	std::shared_ptr<Div> div(
        new Div(
            this->lhs->copy(),
            this->rhs->copy()
        )
	);
	return div;
};

void Div::visit(FormulaVisitor &visitor) const {
	visitor.onDiv(*this);
	this->lhs->visit(visitor);
	this->rhs->visit(visitor);
};


// Inequality.

Inequality::Inequality(
		std::shared_ptr<Expr> lhs,
		std::shared_ptr<Expr> rhs,
		Comparison comp)
		: lhs(lhs), rhs(rhs), comp(comp), size(1) {

    // Set the variables of this inequality.
    this->vars.insert(lhs->getVars().begin(), lhs->getVars().end());
    this->vars.insert(rhs->getVars().begin(), rhs->getVars().end());

    // Set the size of this inequality.
    this->size += this->lhs->getSize();
    this->size += this->rhs->getSize();

    // Set the hash of this inequality.
    std::size_t seed = 5;
	boost::hash_combine(seed, comp);
    boost::hash_combine(seed, this->lhs->getHash());
    boost::hash_combine(seed, this->rhs->getHash());
    this->hash = seed;
}

Inequality::~Inequality() {}

const std::shared_ptr<Expr> Inequality::getLhs() const {
	return this->lhs;
}

const std::shared_ptr<Expr> Inequality::getRhs() const {
	return this->rhs;
}

Comparison Inequality::getComparison() const {
	return this->comp;
}

std::size_t Inequality::getSize() const {
    return this->size;
}

std::size_t Inequality::getHash() const {
	return this->hash;
}

const std::set<std::size_t>& Inequality::getVars() const {
	return this->vars;
}

bool Inequality::subs(std::shared_ptr<Model> model) const {
	mpq_class lhs_value = this->lhs->subs(model);
	mpq_class rhs_value = this->rhs->subs(model);

	switch (this->comp) {
		case Comparison::LT:
			return (lhs_value < rhs_value);
		case Comparison::LE:
			return (lhs_value <= rhs_value);
		case Comparison::GT:
			return (lhs_value > rhs_value);
		case Comparison::GE:
			return (lhs_value >= rhs_value);
		case Comparison::EQ:
			return (lhs_value == rhs_value);
		case Comparison::NE:
			return (lhs_value != rhs_value);
	}

	// This should never happen.
	return false;
}

std::shared_ptr<Inequality> Inequality::renameVars(subst_map &subst) const {
	std::shared_ptr<Inequality> ineq(
        new Inequality(
            this->lhs->renameVars(subst),
            this->rhs->renameVars(subst),
            this->comp
        )
	);
	return ineq;
}

mpq_class Inequality::satDelta(std::shared_ptr<Model> model) const {
	mpq_class lhs_value;
	mpq_class rhs_value;

    try {
        lhs_value = this->lhs->subs(model);
        rhs_value = this->rhs->subs(model);
    } catch (OperationError &e) {
        return std::numeric_limits<double>::max();
    }

	switch (this->comp) {
		case Comparison::LT:
			if (lhs_value >= rhs_value)
				return (lhs_value - rhs_value) + 1;
			break;
		case Comparison::LE:
			if (lhs_value > rhs_value)
				return (lhs_value - rhs_value);
			break;
		case Comparison::GT:
			if (lhs_value <= rhs_value)
				return (rhs_value - lhs_value) + 1;
			break;
		case Comparison::GE:
			if (lhs_value < rhs_value)
				return (rhs_value - lhs_value);
			break;
		case Comparison::EQ:
			if (lhs_value != rhs_value)
				return abs(lhs_value - rhs_value);
			break;
		case Comparison::NE:
			if (lhs_value == rhs_value)
				return 1;
			break;
	}

	return 0;
}

std::string Inequality::toSmt() const {
	std::string comparison = "";

	switch (this->comp) {
		case Comparison::LT:
			comparison = "<";
			break;
		case Comparison::LE:
			comparison = "<=";
			break;
		case Comparison::GT:
			comparison = ">";
			break;
		case Comparison::GE:
			comparison = ">=";
			break;
		case Comparison::EQ:
			comparison = "=";
			break;
		case Comparison::NE:
			comparison = "distinct";
			break;
	}

	return std::string("")
	+ "("
	+ comparison
	+ " " + this->lhs->toSmt()
	+ " " + this->rhs->toSmt()
	+ ")";
}

std::string Inequality::toSexpr() const {
	std::string comparison = "";

	switch (this->comp) {
		case Comparison::LT:
			comparison = "lt";
			break;
		case Comparison::LE:
			comparison = "le";
			break;
		case Comparison::GT:
			comparison = "gt";
			break;
		case Comparison::GE:
			comparison = "ge";
			break;
		case Comparison::EQ:
			comparison = "eq";
			break;
		case Comparison::NE:
			comparison = "ne";
			break;
	}

	return std::string("")
	+ comparison
	+ " " + this->lhs->toSexpr()
	+ " " + this->rhs->toSexpr();
}

std::shared_ptr<Inequality> Inequality::copy() const {
	std::shared_ptr<Inequality> ineq(
			new Inequality(
					this->lhs->copy(),
					this->rhs->copy(),
					this->comp
			)
	);
	return ineq;
};

void Inequality::visit(FormulaVisitor &visitor) const {
	visitor.onInequality(*this);
	this->lhs->visit(visitor);
	this->rhs->visit(visitor);
};



// Clause.

Clause::Clause(std::vector<std::shared_ptr<Inequality> > &ineqs) : size(1) {
	// Save the clauses and inequalities and calculate the hash of this clause.
	std::size_t seed = 6;
	for (auto it=ineqs.begin(); it!=ineqs.end(); ++it) {
		this->ineqs.push_back(*it);
        this->vars.insert((*it)->getVars().begin(), (*it)->getVars().end());
        this->size += (*it)->getSize();
        boost::hash_combine(seed, (*it)->getHash());
	}
    this->hash = seed;
}

Clause::~Clause() {}

const std::vector<std::shared_ptr<Inequality> >& Clause::getIneqs() const {
	return this->ineqs;
}

std::size_t Clause::getSize() const {
    return this->size;
}

std::size_t Clause::getHash() const {
	return this->hash;
}

const std::set<std::size_t>& Clause::getVars() const {
    return this->vars;
}

bool Clause::subs(std::shared_ptr<Model> model) const {
	for (auto it=this->ineqs.begin(); it!=this->ineqs.end(); ++it) {
		if ((*it)->subs(model)) {
			return true;
		}
	}
	return false;
}

std::shared_ptr<Clause> Clause::renameVars(subst_map &subst) const {
	std::vector<std::shared_ptr<Inequality> > ineqs;
	for (auto it=this->ineqs.begin(); it!=this->ineqs.end(); ++it) {
		ineqs.push_back((*it)->renameVars(subst));
	}
	return std::shared_ptr<Clause>(new Clause(ineqs));
}

std::size_t Clause::countSatisfiedInequalities(std::shared_ptr<Model> model) const {
	std::size_t result = 0;
	for (auto it=this->ineqs.begin(); it!=this->ineqs.end(); ++it) {
		if ((*it)->subs(model)) {
			++result;
		}
	}
	return result;
}

mpq_class Clause::satDelta(std::shared_ptr<Model> model) const {
	if (this->ineqs.size() <= 0)
		return 0;

	mpq_class result = this->ineqs[0]->satDelta(model);
	for (std::size_t i=1; i<this->ineqs.size(); ++i) {
		mpq_class sat_delta = this->ineqs[i]->satDelta(model);
		if (sat_delta < result) {
			result = sat_delta;
		}
	}
	return result;
}

std::string Clause::toSmt() const {
	if (this->ineqs.size() == 0) {
		return "true";
	} else if (this->ineqs.size() == 1) {
		return this->ineqs[0]->toSmt();
	}

	std::string result = "(or ";
	for (auto it=this->ineqs.begin(); it!=this->ineqs.end(); ++it) {
		if (it != this->ineqs.begin()) {
			result += " ";
		}
		result += (*it)->toSmt();
	}
	result += ")";
	return result;
}

std::string Clause::toSexpr() const {
	std::string result = std::to_string(ineqs.size());
	for (auto it=this->ineqs.begin(); it!=this->ineqs.end(); ++it) {
		result += " " + (*it)->toSexpr();
	}
	return result;
}

std::shared_ptr<Clause> Clause::copy() const {
	std::vector<std::shared_ptr<Inequality> > ineqs;
	for (auto it=this->ineqs.begin(); it!=this->ineqs.end(); ++it) {
		ineqs.push_back((*it)->copy());
	}
	return std::shared_ptr<Clause>(new Clause(ineqs));
};

void Clause::visit(FormulaVisitor &visitor) const {
	visitor.onClause(*this);
	for (auto it=this->ineqs.begin(); it!=this->ineqs.end(); ++it) {
		(*it)->visit(visitor);
	}
};


// Formula.

Formula::Formula(std::vector<std::shared_ptr<Clause> > &clauses) : size(1) {
	// Save the clauses and inequalities and calculate the hash of this clause.
	std::size_t seed = 7;
	for (auto it=clauses.begin(); it!=clauses.end(); ++it) {
		this->clauses.push_back(*it);
        this->vars.insert((*it)->getVars().begin(), (*it)->getVars().end());
        this->size += (*it)->getSize();
		boost::hash_combine(seed, (*it)->getHash());
		this->clauses_hashset.insert((*it)->getHash());
	}
    this->hash = seed;
}

Formula::~Formula() {}

const std::vector<std::shared_ptr<Clause> >& Formula::getClauses() const {
	return this->clauses;
}

std::shared_ptr<Clause> Formula::getClause(std::size_t i) const {
    assert(i >= 0 && i < this->clauses.size());
    return this->clauses[i];
}

const std::set<std::size_t>& Formula::getClausesHashSet() const {
	return this->clauses_hashset;
}

std::size_t Formula::getSize() const {
    return this->size;
}

std::size_t Formula::getHash() const {
	return this->hash;
}

const std::set<std::size_t>& Formula::getVars() const {
	return this->vars;
}

std::shared_ptr<Formula> Formula::renameVars(subst_map &subst) const {
	std::vector<std::shared_ptr<Clause> > clauses;
	for (auto it=this->clauses.begin(); it!=this->clauses.end(); ++it) {
		clauses.push_back((*it)->renameVars(subst));
	}
	return std::shared_ptr<Formula>(new Formula(clauses));
}

std::shared_ptr<Formula> Formula::compress() const {
	const std::set<std::size_t> &vars = this->getVars();

	subst_map subst;
	std::size_t i = 0;
	for (auto it=vars.begin(); it!=vars.end(); ++it, ++i) {
		subst[*it] = i;
	}

	return this->renameVars(subst);
}

// Create a handy typedef for the undirected-graph type.
typedef boost::adjacency_list<
    boost::vecS,
    boost::vecS,
    boost::undirectedS
> UGraph;

void Formula::sliceByVars(std::vector<std::shared_ptr<Formula> > &result) const {
    const std::vector<std::shared_ptr<Clause> > &clauses = this->clauses;

    // If there is only one clause the formula is already sliced.
	if (clauses.size() <= 1) {
		result.push_back(this->copy());
		return;
	}
    
    // Get the variables of this formula.
    const std::set<std::size_t> &vars = this->getVars();

    // Create an undirected graph with a node for each variable of this formula.
	UGraph g(vars.size());
    
    // Connect nodes representing variables that belong to the same clause.
	for (auto cit=clauses.begin(); cit!=clauses.end(); ++cit) {
        const std::set<std::size_t> &clause_vars = (*cit)->getVars();
        if (clause_vars.size() > 1) {
            for (auto itv=std::next(clause_vars.begin()); itv!=clause_vars.end(); ++itv) {
                std::size_t prev_var = *std::prev(itv);
                std::size_t curr_var = *itv;
                add_edge(prev_var, curr_var, g);
            }
        }
	}
    
    // Calculate the connected components of the graph.
	std::vector<int> component(vars.size());
	connected_components(g, &component[0]);
    
    // Create a vector of empty vectors of clauses.
	std::vector<std::vector<std::shared_ptr<Clause> > > sliced_clauses;
	sliced_clauses.resize(clauses.size());
    
    // Fill the empty vectors with the right clauses.
    for (auto it=clauses.begin(); it!=clauses.end(); ++it) {
        const std::set<std::size_t> &clause_vars = (*it)->getVars();
        int c = component[*(clause_vars.begin())];
        sliced_clauses[c].push_back(*it);
    }
    
    // Build the resulting subformulas.
	for (auto it=sliced_clauses.begin(); it!=sliced_clauses.end(); ++it) {
		if (it->size() > 0) {
            std::shared_ptr<Formula> subformula(new Formula(*it));
			result.push_back(subformula->compress());
		}
	}
}

void Formula::sliceByClauses(std::vector<std::shared_ptr<Formula> > &result) const {
	const std::vector<std::shared_ptr<Clause> > &clauses = this->clauses;

	// If there is only one clause the formula is already sliced.
	if (clauses.size() <= 1) {
		result.push_back(this->copy());
		return;
	}

	// Create an undirected graph with a node for each clause of this formula.
	UGraph g(clauses.size());
    
	// Consider all relevant pairs of clauses. If they share variables create an
	// edge between their nodes.
	for (std::size_t i=0; i<clauses.size(); ++i) {
		for (std::size_t j=0; j<i; ++j) {
			const std::set<std::size_t> &c1_vars = this->getClause(i)->getVars();
			const std::set<std::size_t> &c2_vars = this->getClause(j)->getVars();
			if (!disjoint(c1_vars, c2_vars)) {
				add_edge(i, j, g);
			}
		}
	}
    
	// Calculate the connected components of the graph.
	std::vector<int> component(clauses.size());
	connected_components(g, &component[0]);
    
	// Create a vector of empty vector of clauses (one per component).
	std::vector<std::vector<std::shared_ptr<Clause> > > sliced_clauses;
	sliced_clauses.resize(component.size());

	// Fill the empty vectors with the right clauses.
	for (std::size_t i=0; i<component.size(); i++) {
		int j = component[i];
		sliced_clauses[j].push_back(clauses[i]);
	}

	// Build the resulting subformulas.
	for (auto it=sliced_clauses.begin(); it!=sliced_clauses.end(); ++it) {
		if (it->size() > 0) {
            std::shared_ptr<Formula> subformula(new Formula(*it));
			result.push_back(subformula->compress());
		}
	}
}

void Formula::slice(std::vector<std::shared_ptr<Formula> > &result) const {
    this->sliceByVars(result);
    return;
};

bool Formula::sharesModel(std::shared_ptr<Model> model) const {    
	for (auto it=this->clauses.begin(); it!=this->clauses.end(); ++it) {
		if (!(*it)->subs(model)) {
			return false;
		}
	}
	return true;
}

bool Formula::sharesUnsatCore(std::shared_ptr<UnsatCore> core) const {
	const std::vector<std::size_t> &clauses = core->getClauses();
	for (auto it=clauses.begin(); it!=clauses.end(); ++it) {
		if (this->clauses_hashset.find(*it) == this->clauses_hashset.end()) {
			return false;
		}
	}
	return true;
}

std::size_t Formula::countSatisfiedClauses(std::shared_ptr<Model> model) const {
	std::size_t result = 0;
	for (auto it=this->clauses.begin(); it!=this->clauses.end(); ++it) {
		if ((*it)->subs(model)) {
			++result;
		}
	}
	return result;
}

std::size_t Formula::countSatisfiedInequalities(std::shared_ptr<Model> model) const {
	std::size_t result = 0;
	for (auto it=this->clauses.begin(); it!=this->clauses.end(); ++it) {
		result += (*it)->countSatisfiedInequalities(model);
	}
	return result;
}

double Formula::satDelta(std::shared_ptr<Model> model) const {
	mpq_class result = 0;
	for (auto it=this->clauses.begin(); it!=this->clauses.end(); ++it) {
        mpq_class delta = (*it)->satDelta(model);
        if (delta < std::numeric_limits<double>::max()) {
            result += delta;
        } else {
            result = std::numeric_limits<double>::max();
            break;
        }
	}
	return result.get_d();
}

double Formula::normalizedSatDelta(std::shared_ptr<Model> model) const {
    std::size_t nr_clauses = this->clauses.size();
    std::size_t sat_clauses = 0;
	mpq_class result = 0;
	for (auto it=this->clauses.begin(); it!=this->clauses.end(); ++it) {
        mpq_class delta = (*it)->satDelta(model);
        if (delta < std::numeric_limits<double>::max()) {
            if (delta == 0) {
                ++sat_clauses;
            }
            result += delta;
        } else {
            result = std::numeric_limits<double>::max();
            break;
        }
	}
    double delta = result.get_d();
    double weight = static_cast<double>(nr_clauses - sat_clauses);
    weight /= static_cast<double>(nr_clauses);
    delta *= weight;
	return delta;
}

std::string Formula::toSmt() const {
	std::string result = "";

	// Get the variables of this formula.
	const std::set<std::size_t> &vars = this->getVars();

	// Construct the header composed of all variable declarations.
	for (auto it=vars.begin(); it!=vars.end(); ++it) {
		result += "(declare-fun x" + std::to_string(*it) + " () Int)\n";
	}

	// Add the clauses as assertions.
	const std::vector<std::shared_ptr<Clause> > &clauses = this->clauses;
	for (auto it=clauses.begin(); it!=clauses.end(); ++it) {
		if (it != clauses.begin())
			result += "\n";
		result += "(assert " + (*it)->toSmt() + ")";
	}

	return result;
}

std::string Formula::toSexpr() const {
	const std::vector<std::shared_ptr<Clause> > &clauses = this->clauses;
	std::string result = std::to_string(this->clauses.size()) + "\n";
	for (auto it=clauses.begin(); it!=clauses.end(); ++it) {
		if (it != clauses.begin())
			result += "\n";
		result += (*it)->toSexpr();
	}
	return result;
}

void Formula::visit(FormulaVisitor &visitor) const {
	const std::vector<std::shared_ptr<Clause> > &clauses = this->clauses;
	for (auto it=clauses.begin(); it!=clauses.end(); ++it) {
		(*it)->visit(visitor);
	}
}

std::shared_ptr<Formula> Formula::copy() const {
	std::vector<std::shared_ptr<Clause> > clauses;
	for (auto it=this->clauses.begin(); it!=this->clauses.end(); ++it) {
		clauses.push_back((*it)->copy());
	}
	return std::shared_ptr<Formula>(new Formula(clauses));
};
