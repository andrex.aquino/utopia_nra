#ifndef CORE_FORMULA_FORMULA_FACTORY_H_
#define CORE_FORMULA_FORMULA_FACTORY_H_

#include <exception>
#include <istream>
#include <memory>
#include <string>

#include "core/formula/formula.h"


// Exception that may be thrown by formula factories.
class ParsingError: public std::exception {
	public:
		std::string message;

		ParsingError(std::string msg);
		virtual ~ParsingError() throw();

		const char* what() const throw();
};


// A class to create formulas from streams.
class FormulaFactory {
	private:
		// Decodes an expression in SEXPR format.
		std::shared_ptr<Expr> parseExpression(std::istream &input);

  public:
		FormulaFactory();
		virtual ~FormulaFactory();

		// Decodes a formula in SEXPR format.
		std::shared_ptr<Formula> parseFormula(std::istream &input);
};

#endif
