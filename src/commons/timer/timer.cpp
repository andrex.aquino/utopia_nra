#include "timer.h"


Timer::Timer() {}

Timer::~Timer() {}

void Timer::start() {
    this->tp = std::chrono::high_resolution_clock::now();
}

std::chrono::milliseconds::rep Timer::milli() {
    auto end = std::chrono::high_resolution_clock::now();
    auto start = this->tp;
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    return duration.count();
}

std::chrono::microseconds::rep Timer::micro() {
    auto end = std::chrono::high_resolution_clock::now();
    auto start = this->tp;
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    return duration.count();
}

std::chrono::nanoseconds::rep Timer::nano() {
    auto end = std::chrono::high_resolution_clock::now();
    auto start = this->tp;
    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    return duration.count();
}
