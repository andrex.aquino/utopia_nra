#ifndef COMMONS_TED_ZSTED_H_
#define COMMONS_TED_ZSTED_H_

#include <functional>
#include <memory>
#include <string>
#include <vector>


class Node {
  private:
    std::string label;
    std::vector<std::shared_ptr<Node> > children;

  public:
    // Creates a node with label `label` and no children.
    Node(std::string label);
    
    virtual ~Node();
    
    // Returns the label of this node.
    std::string getLabel();
    
    // Returns the children of this node.
    std::vector<std::shared_ptr<Node> >& getChildren();
    
    // Adds the node `node` as a children of this node.
    void addChild(std::shared_ptr<Node> node);
};

// Returns the tree-edit-distance of two labeled
// trees using the Zhang-Shasha algorithm.
int distance(
    std::shared_ptr<Node> A,
    std::shared_ptr<Node> B,
    std::function<int(std::shared_ptr<Node>)> insert_cost,
    std::function<int(std::shared_ptr<Node>)> remove_cost,
    std::function<int(std::shared_ptr<Node>, std::shared_ptr<Node>)> update_cost);

#endif