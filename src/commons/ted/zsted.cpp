#include "zsted.h"

#include <algorithm> // includes `std::sort`.
#include <deque>
#include <map>
#include <stack>
#include <tuple>

#include "commons/matrix/matrix.h"


// Node.

Node::Node(std::string label) : label(label) {}

Node::~Node() {}

std::string Node::getLabel() {
    return this->label;
}

std::vector<std::shared_ptr<Node> >& Node::getChildren() {
    return this->children;
}

void Node::addChild(std::shared_ptr<Node> node) {
    this->children.push_back(node);
}


// Annotated tree.

class AnnotatedTree {
  public:
    std::shared_ptr<Node> root;
    std::vector<std::shared_ptr<Node> > nodes;
    std::vector<int> ids;
    std::vector<int> lmds;
    std::vector<int> keyroots;
  
    AnnotatedTree(std::shared_ptr<Node> root) : root(root) {
        // Create `stack`.
        std::stack<
            std::pair<
                std::shared_ptr<Node>,
                std::shared_ptr<std::deque<int> >
            >
        > stack;
        
        // Create `pstack`.
        std::stack<
            std::tuple<
                std::shared_ptr<Node>,
                int,
                std::shared_ptr<std::deque<int> >
            >
        > pstack;
        
        std::shared_ptr<std::deque<int> > deque(new std::deque<int>());
        stack.push(std::make_pair(root, deque));
        int j=0;
        while (stack.size() > 0) {
            std::shared_ptr<Node> n;
            std::shared_ptr<std::deque<int> > anc;
            std::tie(n, anc) = stack.top();
            stack.pop();
            int nid = j;
            std::vector<std::shared_ptr<Node> > children = n->getChildren();
            for (auto cit=children.begin(); cit!=children.end(); ++cit) {
                // Copy `anc` deque in `a`.
                std::shared_ptr<std::deque<int> > a(new std::deque<int>(*anc));
                a->push_front(nid);
                stack.push(std::make_pair(*cit, a));
            }
            pstack.push(std::make_tuple(n, nid, anc));
            ++j;
        }
        std::map<int, int> lmds;
        std::map<int, int> keyroots;
        int i=0;
        while (pstack.size() > 0) {
            std::shared_ptr<Node> n;
            int nid;
            std::shared_ptr<std::deque<int> > anc;
            std::tie(n, nid, anc) = pstack.top();
            pstack.pop();
            this->nodes.push_back(n);
            this->ids.push_back(nid);
            int lmd;
            if (n->getChildren().size() == 0) {
                lmd = i;
                for (auto ait=anc->begin(); ait!=anc->end(); ++ait) {
                    if (lmds.find(*ait) == lmds.end()) {
                        lmds[*ait] = i;
                    } else {
                        break;
                    }
                }
            } else {
                lmd = lmds[nid];
            }
            this->lmds.push_back(lmd);
            keyroots[lmd] = i;
            ++i;
        }
        
        
        // Init `this->keyroots` with the sorted values from the `keyroots` map.
        std::vector<int> values;
        for(auto it=keyroots.begin(); it!=keyroots.end(); ++it) {
            values.push_back(it->second);
        }
        std::sort(values.begin(), values.end());
        
        for (auto it=values.begin(); it!=values.end(); ++it) {
            this->keyroots.push_back(*it);
        }
    }
};


// Distance.

void treedist(
        AnnotatedTree &A,
        AnnotatedTree &B,
        std::function<int(std::shared_ptr<Node>)> insert_cost,
        std::function<int(std::shared_ptr<Node>)> remove_cost,
        std::function<int(std::shared_ptr<Node>, std::shared_ptr<Node>)> update_cost,
        int i,
        int j,
        Matrix &treedists) {

    std::vector<int> &Al = A.lmds;
    std::vector<int> &Bl = B.lmds;
    std::vector<std::shared_ptr<Node> > &An = A.nodes;
    std::vector<std::shared_ptr<Node> > &Bn = B.nodes;
    
    int m = (i - Al[i]) + 2;
    int n = (j - Bl[j]) + 2;
    Matrix fd(m, n);
    
    int ioff = Al[i] - 1;
    int joff = Bl[j] - 1;
    
    // δ(l(i1)..i, θ) = δ(l(1i)..1-1, θ) + γ(v → λ)
    for (int x=1; x<m; ++x) {
        int value = fd.get(x-1, 0) + remove_cost(An[x+ioff]);
        fd.set(x, 0, value);
    }
    
    // δ(θ, l(j1)..j) = δ(θ, l(j1)..j-1) + γ(λ → w)
    for (int y=1; y<n; ++y) {
        int value = fd.get(0, y-1) + insert_cost(Bn[y+joff]);
        fd.set(0, y, value);
    }
    
    for (int x=1; x<m; ++x) {
        for (int y=1; y<n; ++y) {
            // only need to check if x is an ancestor of i
            // and y is an ancestor of j
            if (Al[i] == Al[x+ioff] && Bl[j] == Bl[y+joff]) {
                //                   +-
                //                   | δ(l(i1)..i-1, l(j1)..j) + γ(v → λ)
                // δ(F1 , F2 ) = min-+ δ(l(i1)..i , l(j1)..j-1) + γ(λ → w)
                //                   | δ(l(i1)..i-1, l(j1)..j-1) + γ(v → w)
                //                   +-
                int value = std::min(
                    fd.get(x-1, y) + remove_cost(An[x+ioff]),
                    std::min(
                        fd.get(x, y-1) + insert_cost(Bn[y+joff]),
                        fd.get(x-1, y-1) + update_cost(An[x+ioff], Bn[y+joff])
                    )
                );
                fd.set(x, y, value);
                treedists.set(x+ioff, y+joff, fd.get(x, y));
            } else {
                //                   +-
                //                   | δ(l(i1)..i-1, l(j1)..j) + γ(v → λ)
                // δ(F1 , F2 ) = min-+ δ(l(i1)..i , l(j1)..j-1) + γ(λ → w)
                //                   | δ(l(i1)..l(i)-1, l(j1)..l(j)-1)
                //                   |                     + treedist(i1,j1)
                //                   +-
                int p = (Al[x+ioff] - 1) - ioff;
                int q = (Bl[y+joff] - 1) - joff;
                int value = std::min(
                    fd.get(x-1, y) + remove_cost(An[x+ioff]),
                    std::min(
                        fd.get(x, y-1) + insert_cost(Bn[y+joff]),
                        fd.get(p, q) + treedists.get(x+ioff, y+joff)
                    )
                );
                fd.set(x, y, value);
            }
        }
    }
}

int distance(
        std::shared_ptr<Node> tree1,
        std::shared_ptr<Node> tree2,
        std::function<int(std::shared_ptr<Node>)> insert_cost,
        std::function<int(std::shared_ptr<Node>)> remove_cost,
        std::function<int(std::shared_ptr<Node>, std::shared_ptr<Node>)> update_cost) {

    AnnotatedTree A(tree1);
    AnnotatedTree B(tree2);
    Matrix treedists(A.nodes.size(), B.nodes.size());

    for (auto it1=A.keyroots.begin(); it1!=A.keyroots.end(); ++it1) {
        for (auto it2=B.keyroots.begin(); it2!=B.keyroots.end(); ++it2) {
            treedist(
                A,
                B,
                insert_cost,
                remove_cost,
                update_cost,
                *it1,
                *it2,
                treedists
            );
        }
    }
    
    int result = treedists.get(A.nodes.size()-1, B.nodes.size()-1);
    return result;
}