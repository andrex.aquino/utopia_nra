#include "matrix.h"

#include <assert.h>


int Matrix::mapRow(int i) const {
    assert(i >= -this->rows && i < this->rows);
    if (i < 0) {
        return (this->rows + i);
    }
    return i;
}
    
int Matrix::mapColumn(int j) const {
    assert(j >= -this->cols && j < this->cols);
    if (j < 0) {
        return (this->cols + j);
    }
    return j;
}

int Matrix::index(int i, int j) const {
    i = this->mapRow(i);
    j = this->mapColumn(j);
    
    return (i * this->cols) + j;
}
    
Matrix::Matrix(int rows, int cols) : rows(rows), cols(cols) {
    assert(rows > 0 && cols > 0);
    
    // Allocate matrix and set all values to zero.
    int *matrix = new int[rows * cols];
    for (int i=0; i<rows*cols; ++i) {
        matrix[i] = 0;
    }
    
    this->data = matrix;
}

Matrix::~Matrix() {
    delete[] this->data;
}

int Matrix::get(int i, int j) const {
    return this->data[this->index(i,j)];
}

void Matrix::set(int i, int j, int value) {
    this->data[this->index(i,j)] = value;
}