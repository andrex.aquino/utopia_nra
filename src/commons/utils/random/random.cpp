#include "random.h"

#include <assert.h>
#include <cstdlib> // includes std::rand


int randint(int start, int end) {
    assert(start <= end);
    
    if (start == end)
        return start;
    
    int offset = std::rand() % (end - start);
    return start + offset;
}