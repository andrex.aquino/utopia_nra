#ifndef COMMONS_RANDOM_RANDOM_H
#define COMMONS_RANDOM_RANDOM_H

// Returns a uniformely distributed random number in the interval [`start`,`end`)
int randint(int start, int end);

#endif