# Utopia (NRA)

Utopia (NRA) is a caching framework targeting formulas from the quantifier-free non-linear real arithmetic logic. As opposed to other caching frameworks that compare the syntax trees of two formulas to determine whether one implies, is contained in or is equivalent to the other to reuse solutions, Utopia exploits a heuristic called sat-delta to identify potentially reusable solutions for a target formula.

*Notice*: Despite the name, Utopia (NRA) targets quantifier-free formulas from the non-linear real arithmetic logic (commonly known as QFNRA).

## Installation

Utopia depends on the Boost and the Z3 libraries. 
The provided makefile to compile Utopia assumes the existence of a folder named `lib` at the same level of the `utopia_nra` folder containing a compiled version of the [Boost](http://www.boost.org) library (v.1.62.0) in a subfolder named `boost` and a compiled version of the [Z3](https://github.com/Z3Prover/z3) library (v.4.4.2) in a subfolder named `z3`.

The makefile also assumes the existence of a folder named `bin` in the main folder of the project (`utopia_nra`).

The provided makefile has been tested on MacOS X El Capitan and Ubuntu Linux 16.04.

## Running Utopia

After compilation, the Utopia binary is located in the folder `bin`.
Currently, the compilation of Utopia produces one binary named *utopia*: the main tool to experiment with benchmarks of formulas.

To run this binary it is necessary to give it access to the Boost and Z3 dynamic libraries. 
These are usually located in the folders `lib/boost/stage/lib/` and `lib/z3/build`.

Simply add them to the DYLD_LIBRARY_PATH or LD_LIBRARY_PATH environment variables on MacOs and Linux, respectively.

Both binaries can be invoked with the --help flag to list all valid parameters.

The simplest way to run Utopia on a benchmark file of formulas `ds` is the following:

    ./bin/utopia --solver=satdelta_C3 --tier1-candidates=10 --tier2-candidates=10 --dataset=ds --verbose

The flag --verbose can be omitted to produce only a small summary of the output. 
When ran with the --verbose flag, Utopia prints statistics about the cache after every formula has been solved.

Utopia exploits formula slicing by default, to disable the feature simply add the --no-slicing flag.

## Benchmark

You can download a benchmark of formulas in the format accepted by Utopia [here](https://drive.google.com/open?id=0B5BAqcpCRvEPNzBGc1Rnd0YyMjQ).

These formulas were produced by running the Gk-tail model checker on real programs.
If the benchmark is stored in the folder `data` of the Utopia project, the provided makefile in the same folder can be used to automatically run a large number of experiments on them. Beware that this might take a long time (even hours depending on the machine running the experiments). The experiments can be parallelized invoking the command: 

    make -j NUMBER_OF_THREADS

## Running tests

The tests for Utopia are located in the folder `tests`. 
They can be run as follows:

```bash
cd tests;
make && ./run.sh
```